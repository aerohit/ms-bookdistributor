package com.aerohitsaxena.bookdistributor

import argonaut._
import Argonaut._
import com.aerohitsaxena.kafka.KafkaMarshaller
import com.aerohitsaxena.commons.kafkaHelpers._

case class BookDistributorRegistered(bookDistributorId: String, name: String, email: String)

case class BookStockAdded(
  bookDistributorId: String,
  stockId: String,
  isbn: String,
  title: String,
  authors: List[String],
  price: Int,
  daysToDeliver: Int
)

object BookDistributorRegistered {
  implicit lazy val codec: CodecJson[BookDistributorRegistered] =
    casecodec3(BookDistributorRegistered.apply, BookDistributorRegistered.unapply)(
      "book_distributor_id",
      "name",
      "email"
    )

  implicit lazy val kafkaMarshaller: KafkaMarshaller[BookDistributorRegistered] = codec.kafkaMarshaller
}

object BookStockAdded {
  implicit lazy val codec: CodecJson[BookStockAdded] =
    casecodec7(BookStockAdded.apply, BookStockAdded.unapply)(
      "book_distributor_id",
      "stock_id",
      "isbn",
      "title",
      "authors",
      "price",
      "days_to_deliver"
    )

  implicit lazy val kafkaMarshaller: KafkaMarshaller[BookStockAdded] = codec.kafkaMarshaller
}

package com.aerohitsaxena.bookdistributor

import java.util.UUID

import akka.actor.typed.ActorRef
import akka.actor.ActorSystem
import akka.actor.Scheduler
import akka.cluster.sharding.typed.scaladsl.EntityRef
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.aerohitsaxena.bookdistributor.BookDistributorActor._
import com.aerohitsaxena.bookdistributor.BookDistributorRegistered._
import com.aerohitsaxena.kafka.KafkaProducer

import scala.concurrent.duration._
import scala.concurrent.Await
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.language.postfixOps

trait BookDistributionService {
  def register(request: DistributorRegistrationRequest): Future[DistributorRegistrationResponse]

  def distributorFor(accessToken: AccessToken): Future[Option[String]]

  def addStock(accessToken: AccessToken, request: AddStockRequest): Future[AddStockResponse]
}

class DefaultBookDistributorService(
  actorForEntityId: AccessToken => EntityRef[BookDistributorActor.Command],
  distributorRegisteredPublisher: ActorRef[BookDistributorRegistered],
  stockAddedPublisher: ActorRef[BookStockAdded]
)(implicit timeout: Timeout, scheduler: Scheduler, ec: ExecutionContext)
    extends BookDistributionService {

  def register(request: DistributorRegistrationRequest): Future[DistributorRegistrationResponse] = {
    val id          = UUID.randomUUID().toString
    val accessToken = UUID.randomUUID().toString
    val actor       = actorForEntityId(accessToken)

    (actor ? registrationRequest(id, request.name, request.email, accessToken)).map {
      case RegisteredDistributor(id, name, email) =>
        distributorRegisteredPublisher ! BookDistributorRegistered(id, name, email)
        DistributorRegistrationResponse(id, name, email, accessToken)
    }
  }

  override def distributorFor(accessToken: AccessToken): Future[Option[String]] = {
    val actor = actorForEntityId(accessToken)
    (actor ? fetchRequest(accessToken)).map({
      case FetchedDistributor(bookDistributorId, _, _) =>
        Option(bookDistributorId)
      case NoDistributorFound =>
        None
    })
  }

  override def addStock(accessToken: AccessToken, request: AddStockRequest): Future[AddStockResponse] = {
    val stockId = UUID.randomUUID().toString
    val actor   = actorForEntityId(accessToken)
    (actor ? addStockRequest(stockId, request)).map {
      case StockAdded(bookDistributorId) =>
        stockAddedPublisher ! BookStockAdded(
          bookDistributorId,
          stockId,
          request.isbn,
          request.title,
          request.authors,
          request.price,
          request.daysToDeliver
        )
        AddStockResponse(stockId)
    }
  }

  private def registrationRequest(bookDistributorId: String, name: String, email: String, accessToken: AccessToken)(
    replyTo: ActorRef[RegistrationResponse]) =
    RegisterDistributor(bookDistributorId, name, email, accessToken, replyTo)

  private def fetchRequest(accessToken: AccessToken)(replyTo: ActorRef[FetchDistributorResponse]) = {
    FetchDistributor(accessToken, replyTo)
  }

  private def addStockRequest(stockId: String, req: AddStockRequest)(
    replyTo: ActorRef[BookDistributorActor.AddStockResponse]) =
    AddStock(
      BookDistributorActor.BookStock(stockId, req.isbn, req.title, req.authors, req.price, req.daysToDeliver),
      replyTo)
}

object DefaultBookDistributorService {
  def apply()(implicit actorSystem: ActorSystem, mat: ActorMaterializer): DefaultBookDistributorService = {
    val shard               = new BookDistributorActorShard(actorSystem).createShard()
    val actorRefForEntityId = Await.result(shard, 60 seconds)

    // TODO: topic name should be created from event
    val distributorRegisteredPublisher: ActorRef[BookDistributorRegistered] =
      KafkaProducer.create("bookdistributorregistered")
    val stockAddedPublisher: ActorRef[BookStockAdded] =
      KafkaProducer.create("bookdistributorregistered")

    new DefaultBookDistributorService(
      (accessToken: AccessToken) => actorRefForEntityId(accessToken.toString),
      distributorRegisteredPublisher,
      stockAddedPublisher)(
      Timeout(10 seconds),
      actorSystem.scheduler,
      actorSystem.dispatcher
    )
  }
}

package com.aerohitsaxena.bookdistributor

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.Credentials
import com.aerohitsaxena.commons.DefaultConfigurationProvider
import com.aerohitsaxena.commons.RouteDefinition
import com.aerohitsaxena.commons.WebServer
import com.aerohitsaxena.logging.LoggingBehaviour
import de.heikoseeberger.akkahttpargonaut.ArgonautSupport
import scala.concurrent.Future

object BookDistributorWebService
    extends WebServer
    with BookDistributorRoute
    with DefaultConfigurationProvider
    with ArgonautSupport {
  override val bookDistributorService = DefaultBookDistributorService()
}

trait BookDistributorRoute extends RouteDefinition { self: LoggingBehaviour with ArgonautSupport =>
  def bookDistributorService: BookDistributionService

  lazy val route: Route =
    post {
      pathPrefix("bookdistributor") {
        path("register") {
          entity(as[DistributorRegistrationRequest]) { registrationRequest =>
            logger.info(s"Received registration request $registrationRequest")
            complete(bookDistributorService.register(registrationRequest))
          }
        } ~ path("addstock") {
          (authenticateOAuth2Async(
            "bookdistributor",
            Authentication.tokenAuthenticator(bookDistributorService.distributorFor)) & entity(as[AddStockRequest])) {
            (accessToken, addStockRequest) =>
              logger.info(s"Received add stock request $addStockRequest")
              complete(bookDistributorService.addStock(accessToken, addStockRequest))
          }
        }
      }
    }
}

object Authentication {
  // TODO: this is a fake implementation
  def tokenAuthenticator(verifyAccessToken: AccessToken => Future[Option[String]])(
    credentials: Credentials): Future[Option[String]] =
    credentials match {
      case Credentials.Provided(id) =>
//        Future.successful(Some(id))
        verifyAccessToken(id)
      case _ =>
        Future.successful(None)
    }
}

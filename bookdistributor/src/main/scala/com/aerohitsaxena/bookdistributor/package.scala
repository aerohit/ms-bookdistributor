package com.aerohitsaxena

import argonaut._
import Argonaut._
import scala.language.implicitConversions

package object bookdistributor {
  case class HiddenValue[T](value: T) {
    override def toString: String = s"SecuredValue%${hashCode()}"
  }

  type HiddenString = HiddenValue[String]

  object HiddenValue {
    implicit def stringToHidden(value: String): HiddenString = HiddenValue(value)
  }

  implicit val hiddenStringCaseCodec: CodecJson[HiddenString] = CodecJson(
    (hs: HiddenString) => jString(hs.value),
    c =>
      for {
        value <- c.as[String]
      } yield HiddenValue(value)
  )

  type AccessToken = HiddenString
}

package com.aerohitsaxena.bookdistributor

import akka.actor.ActorSystem
import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.persistence.typed.scaladsl.Effect
import akka.persistence.typed.scaladsl.PersistentBehaviors
import akka.persistence.typed.scaladsl.PersistentBehaviors.CommandHandler
import com.aerohitsaxena.commons.TypedShardProvider
import com.aerohitsaxena.logging.LoggingBehaviour

object BookDistributorActor extends LoggingBehaviour {
  sealed trait Command

  case class RegisterDistributor(
    bookDistributorId: String,
    name: String,
    email: String,
    accessToken: AccessToken,
    replyTo: ActorRef[RegistrationResponse])
      extends Command
  sealed trait RegistrationResponse
  case class RegisteredDistributor(bookDistributorId: String, name: String, email: String) extends RegistrationResponse

  case class FetchDistributor(accessToken: AccessToken, replyTo: ActorRef[FetchDistributorResponse]) extends Command
  sealed trait FetchDistributorResponse
  case object NoDistributorFound                                                        extends FetchDistributorResponse
  case class FetchedDistributor(bookDistributorId: String, name: String, email: String) extends FetchDistributorResponse

  case class AddStock(stock: BookStock, replyTo: ActorRef[AddStockResponse]) extends Command

  sealed trait AddStockResponse
  case class StockAdded(bookDistributorId: String) extends AddStockResponse

  case object KillActor extends Command

  sealed trait Event
  case class StoredDistributor(bookDistributorId: String, name: String, email: String, accessToken: AccessToken)
      extends Event
  case class StoredStock(stock: BookStock) extends Event

  sealed trait State {
    def register(bookDistributorId: String, name: String, email: String, accessToken: AccessToken): State
    def addStock(stock: BookStock): State
  }

  // TODO: Re-design state
  // Shouldn't be possible to register more than once
  // Shouldn't allow to add stock unless registered
  case class DistributorActorState(distributor: Option[Distributor] = None) extends State {
    override def register(bookDistributorId: String, name: String, email: String, accessToken: AccessToken): State =
      DistributorActorState(Option(Distributor(bookDistributorId, name, email, accessToken)))

    override def addStock(stock: BookStock): State =
      DistributorActorState(distributor.map(d => d.copy(stock = stock +: d.stock)))

    def getDistributor: FetchDistributorResponse = distributor match {
      case Some(dist) =>
        FetchedDistributor(dist.bookDistributorId, dist.name, dist.email)
      case None =>
        NoDistributorFound
    }
  }

  case class Distributor(
    bookDistributorId: String,
    name: String,
    email: String,
    accessToken: AccessToken,
    stock: Seq[BookStock] = Seq.empty)
  case class BookStock(
    stockId: String,
    isbn: String,
    title: String,
    authors: List[String],
    price: Int,
    daysToDeliver: Int)

  private val commandHandler: CommandHandler[Command, Event, State] = (_, state, cmd) => {
    logger.info(s"Received command $cmd")
    cmd match {
      case RegisterDistributor(bookDistributorId, name, email, accessToken, replyTo) =>
        val event = StoredDistributor(bookDistributorId, name, email, accessToken)
        Effect.persist(event) andThen {
          replyTo ! RegisteredDistributor(bookDistributorId, name, email)
        }
      case FetchDistributor(_, replyTo) =>
        val response = state match {
          case das: DistributorActorState => das.getDistributor
        }
        replyTo ! response
        Effect.none
      case AddStock(stock, replyTo) =>
        val event = StoredStock(stock)
        Effect.persist(event) andThen {
          replyTo ! StockAdded("fake-book-distributor-id")
        }
      case KillActor =>
        Effect.stop
    }
  }

  private val eventHandler: (State, Event) => State = (state, event) =>
    event match {
      case StoredDistributor(bookDistributorId, name, email, accessToken) =>
        state.register(bookDistributorId, name, email, accessToken)
      case StoredStock(stock) =>
        state.addStock(stock)
  }

  def behaviour(entityId: String): Behavior[Command] = PersistentBehaviors.receive[Command, Event, State](
    persistenceId = s"Distributor-$entityId",
    initialState = DistributorActorState(),
    commandHandler = commandHandler,
    eventHandler = eventHandler
  )

  val EntityName = "BookDistributor"
}

class BookDistributorActorShard(val actorSystem: ActorSystem)
    extends TypedShardProvider[BookDistributorActor.Command]
    with LoggingBehaviour {
  override def behaviour(entityId: String): Behavior[BookDistributorActor.Command] =
    BookDistributorActor.behaviour(entityId)

  override val shardName: String = BookDistributorActor.EntityName

  override val stopMessage: BookDistributorActor.Command = BookDistributorActor.KillActor

  override val numberOfShards: Int = 100
}

package com.aerohitsaxena.bookdistributor

import argonaut._
import Argonaut._

case class DistributorRegistrationRequest(name: String, email: String)
case class DistributorRegistrationResponse(
  bookDistributorId: String,
  name: String,
  email: String,
  accessToken: AccessToken
)

case class AddStockRequest(isbn: String, title: String, authors: List[String], price: Int, daysToDeliver: Int)
case class AddStockResponse(stockId: String)

object DistributorRegistrationRequest {
  implicit val DistributorRegistrationRequestCodecJson =
    casecodec2(DistributorRegistrationRequest.apply, DistributorRegistrationRequest.unapply)("name", "email")
}

object DistributorRegistrationResponse {
  implicit val DistributorRegistrationResponseCodecJson =
    casecodec4(DistributorRegistrationResponse.apply, DistributorRegistrationResponse.unapply)(
      "book_distributor_id",
      "name",
      "email",
      "access_token"
    )
}

object AddStockRequest {
  implicit val addStockRequestCodec =
    casecodec5(AddStockRequest.apply, AddStockRequest.unapply)("isbn", "title", "authors", "price", "days_to_deliver")
}

object AddStockResponse {
  implicit val addStockResponseCodec =
    casecodec1(AddStockResponse.apply, AddStockResponse.unapply)("stock_id")
}

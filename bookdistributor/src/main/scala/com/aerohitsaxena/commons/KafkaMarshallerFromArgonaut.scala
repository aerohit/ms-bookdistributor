package com.aerohitsaxena.commons

import argonaut.EncodeJson
import com.aerohitsaxena.kafka.KafkaMarshaller

// TODO:
//object KafkaMarshallerFromArgonaut {
//  def kafkaMarshaller[E](implicit enc: EncodeJson[E]): KafkaMarshaller[E] =
//    (msg: E) => enc.encode(msg).toString()
//}

object kafkaHelpers {
  implicit class KafkaMarshallerFromArgonaut[E](enc: EncodeJson[E]) {
    def kafkaMarshaller: KafkaMarshaller[E] =
      (msg: E) => enc.encode(msg).toString()
  }
}

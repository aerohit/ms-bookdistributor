package com.aerohitsaxena.commons

import akka.actor.ActorSystem
import akka.cluster.ClusterEvent.ClusterDomainEvent
import akka.cluster.Cluster
import akka.cluster.ClusterEvent
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.RouteResult
import akka.http.scaladsl.server.directives.DebuggingDirectives
import akka.http.scaladsl.server.directives.LogEntry
import akka.management.AkkaManagement
import akka.management.cluster.bootstrap.ClusterBootstrap
import akka.stream.ActorMaterializer
import com.aerohitsaxena.bookdistributor.SimpleClusterListener
import com.aerohitsaxena.logging.LoggingBehaviour
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory

import scala.concurrent.Await
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Failure
import scala.util.Success

trait ActorSystemProvider { self: ConfigurationProvider with LoggingBehaviour =>
  implicit lazy val actorSystem: ActorSystem = ActorSystem(
    config.getString("server.name"), // TODO: implement a more robust way of passing these configurations around
    config
  )

  sys.addShutdownHook {
    val systemName = actorSystem.name
    logger.info(s"Terminating actor system ${systemName}")
    Await.result(actorSystem.terminate(), 30 seconds)
    logger.info(s"Terminated actor system ${systemName}. Bye")
  }
}

trait ConfigurationProvider {
  val config: Config
}

trait DefaultConfigurationProvider extends ConfigurationProvider {
  implicit lazy val config: Config = ConfigFactory.load()
}

trait ActorMaterializerProvider { self: ActorSystemProvider =>
  implicit lazy val materializer: ActorMaterializer = ActorMaterializer()
}

trait ExecutionContextProvider { self: ActorSystemProvider =>
  implicit lazy val executionContext: ExecutionContextExecutor = actorSystem.dispatcher
}

trait RouteDefinition {
  def route: Route
}

trait RequestResponseLogging {
  private def responseOrRejectionsLogger: HttpRequest => RouteResult => Option[LogEntry] = req => {
    case RouteResult.Complete(response) =>
      toLogEntry(s"${reqToString(req)} responded with ${response.status}")
    case RouteResult.Rejected(rejections) =>
      toLogEntry(s"${reqToString(req)} rejected because $rejections")
  }

  private def reqToString(req: HttpRequest): String =
    s"${req.method.name} ${req.uri.path}"

  private def toLogEntry(logString: String) = Option(LogEntry(logString, Logging.InfoLevel))

  lazy val requestResponseLogger = DebuggingDirectives.logRequestResult(responseOrRejectionsLogger)
}

trait ClusteredService { self: ActorSystemProvider with LoggingBehaviour =>
  def bootup(): Unit

  val cluster = Cluster(actorSystem)

  AkkaManagement(actorSystem).start()
  ClusterBootstrap(actorSystem).start()

  cluster.subscribe(
    actorSystem.actorOf(akka.actor.Props[SimpleClusterListener]),
    ClusterEvent.InitialStateAsEvents,
    classOf[ClusterDomainEvent]
  )

  cluster.registerOnMemberUp {
    logger.info(s"This member is up, calling bootup")
    bootup()
  }
}

trait RunnableRoute extends RouteDefinition {
  self: ActorSystemProvider
    with ActorMaterializerProvider
    with ExecutionContextProvider
    with ConfigurationProvider
    with LoggingBehaviour
    with RequestResponseLogging =>

//  private lazy val port: Int = config.getInt("server.port")
  private lazy val port: Int = 10001

  def bindRoutes(): Unit = {
    logger.info(s"Trying to start service on port $port")
    val bindingFuture: Future[Http.ServerBinding] = Http().bindAndHandle(
      requestResponseLogger(route),
      "0.0.0.0",
      port
    )

    sys.addShutdownHook {
      bindingFuture.foreach { binding: Http.ServerBinding =>
        logger.info(s"Unbinding server $binding")
        binding.unbind()
      }
    }

    bindingFuture.onComplete {
      case Success(serverBinding) =>
        logger.info(s"Server bound to ${serverBinding.localAddress}")

      case Failure(ex) =>
        logger.error(s"Failed to bind server on port $port, exiting.: $ex")
        actorSystem.terminate()
    }
  }
}

trait WebServer
    extends App
    with ClusteredService
    with RunnableRoute
    with ActorSystemProvider
    with ActorMaterializerProvider
    with ExecutionContextProvider
    with LoggingBehaviour
    with RequestResponseLogging
    with ConfigurationProvider {
  override def bootup(): Unit = {
    bindRoutes()
  }
}

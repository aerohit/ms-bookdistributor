package com.aerohitsaxena.commons

import akka.actor.ActorSystem
import akka.actor.typed
import akka.actor.typed.Behavior
import akka.actor.typed.Props
import akka.actor.typed.scaladsl.adapter._
import akka.cluster.Cluster
import akka.cluster.sharding.typed.ClusterShardingSettings
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.cluster.sharding.typed.scaladsl.EntityRef
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import com.aerohitsaxena.logging.LoggingBehaviour

import scala.concurrent.Future
import scala.concurrent.Promise
import scala.reflect.ClassTag

trait TypedShardProvider[Cmd] { self: LoggingBehaviour =>
  val actorSystem: ActorSystem

  def behaviour(entityId: String): Behavior[Cmd]

  val shardName: String

  def shardingKey(implicit tTag: ClassTag[Cmd]) = EntityTypeKey[Cmd](shardName)

  val stopMessage: Cmd

  val numberOfShards: Int

  lazy val typedActorSystem: typed.ActorSystem[Nothing] = actorSystem.toTyped

  def createShard()(implicit tTag: ClassTag[Cmd]): Future[String => EntityRef[Cmd]] = {
    logger.info(s"Will create shard $shardName after this member is up")

    val promise = Promise[String => EntityRef[Cmd]]

    Cluster(actorSystem).registerOnMemberUp {
      logger.info(s"This member is up, spawning shard $shardName")
      val clusterSharding = ClusterSharding(typedActorSystem)
      val typeKey         = shardingKey

      clusterSharding.spawn[Cmd](
        behavior = entityId => behaviour(entityId),
        props = Props.empty,
        typeKey = typeKey,
        settings = ClusterShardingSettings(typedActorSystem),
        maxNumberOfShards = numberOfShards,
        handOffStopMessage = stopMessage
      )

      promise.success((entityId: String) => clusterSharding.entityRefFor(typeKey, entityId))
    }
    promise.future
  }
}

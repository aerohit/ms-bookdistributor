## The Goal

The project is to try out some cutting edge technology like:

- Akka typed actors, with cluster and persistence
- Akka management for cluster formation

Some other technology used in the project:

- Kafka for publishing events
- Docker for containerization and Kubernetes for orchestration
- Fluentd to pipe logs to ElasticSearch and to be visualized by Kibana.

There are kubernetes deployment files for all the services.


# Follow instructions to start the services:

## Starting minikube


```
minikube delete
minikube start --cpus 4 --memory 8192
```


**Do the following to make sure kafka messages can be published.**

It's described here: https://github.com/kubernetes/minikube/issues/1690

```
minikube ssh
sudo ip link set docker0 promisc on
```

## BookDistributor


```
kc create -f bookdistributor/bookdistributor-cluster.yml
```

**Note:** `kc` is an alias for `kubectl` in the documents below.

## Starting Kafka:


Bringing up cluster:

```
kc create -f kafka-service/zookeeper-service.yml
kc create -f kafka-service/zookeeper-deployment.yml
kc create -f kafka-service/kafka-service.yml
kc create -f kafka-service/kafka-deployment.yml
```

Bringing down cluster:
```
kc delete deploy/zookeeper svc/zookeeper-service
kc delete deploy/kafka svc/kafka-service
```


To test the kafka cluster:

```
echo "Am I receiving this message?" | kafkacat -P -b 192.168.99.100:30092 -t test-topic  # produce message in one terminal
kafkacat -C -b 192.168.99.100:30092 -t test-topic   # consume from a different terminal
```

## Setup EFK stack:

Read the readme on this page: https://github.com/giantswarm/kubernetes-elastic-stack

Copied the file to efk-stack.yaml


```
kubectl apply --filename efk-stack.yaml

kubectl delete --filename efk-stack.yaml
```
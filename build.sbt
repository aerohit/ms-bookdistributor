lazy val akkaStreamsVersion    = "10.1.1"
lazy val akkaVersion           = "2.5.12"
lazy val akkaManagementVersion = "0.12.0"
scalaVersion := "2.12.6"

lazy val compilerFlags = Seq(
  "-deprecation", // Emit warning and location for usages of deprecated APIs.
  "-encoding",
  "utf-8", // Specify character encoding used by source files.
  "-explaintypes", // Explain type errors in more detail.
  "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  "-Xfatal-warnings", // Fail the compilation if there are any warnings.
  "-Xlint:inaccessible", // Warn about inaccessible types in method signatures.
  "-Xlint:infer-any", // Warn when a type argument is inferred to be `Any`.
  "-Ywarn-dead-code", // Warn when dead code is identified.
  "-Ywarn-inaccessible", // Warn about inaccessible types in method signatures.
  "-Ywarn-infer-any", // Warn when a type argument is inferred to be `Any`.
  "-Ywarn-unused:implicits", // Warn if an implicit parameter is unused.
  "-Ywarn-unused:imports", // Warn if an import selector is not referenced.
  "-Ywarn-unused:locals", // Warn if a local definition is unused.
  "-Ywarn-unused:params", // Warn if a value parameter is unused.
  "-Ywarn-unused:patvars", // Warn if a variable bound in a pattern is unused.
  "-Ywarn-unused:privates", // Warn if a private member is unused.
  "-Ywarn-value-discard"
)

lazy val bookdistributor = project
  .settings(
    scalafmtOnCompile in ThisBuild := true,
    scalafmtTestOnCompile in ThisBuild := true,
    dockerRepository := Some("aerohit"),
    dockerBaseImage := "anapsix/alpine-java",
    dockerUpdateLatest := true,
    git.useGitDescribe := true,
    scalacOptions ++= compilerFlags,
    libraryDependencies ++= Seq(
      "com.typesafe.akka"             %% "akka-actor"                        % akkaVersion,
      "com.typesafe.akka"             %% "akka-stream"                       % akkaVersion,
      "com.typesafe.akka"             %% "akka-slf4j"                        % akkaVersion,
      "com.typesafe.akka"             %% "akka-persistence-typed"            % akkaVersion,
      "com.typesafe.akka"             %% "akka-cluster-sharding-typed"       % akkaVersion,
      "com.typesafe.akka"             %% "akka-http"                         % akkaStreamsVersion,
      "com.lightbend.akka.management" %% "akka-management"                   % akkaManagementVersion,
      "com.lightbend.akka.management" %% "akka-management-cluster-bootstrap" % akkaManagementVersion,
      "com.lightbend.akka.discovery"  %% "akka-discovery-kubernetes-api"     % akkaManagementVersion,
      "org.fusesource.leveldbjni"     % "leveldbjni-all"                     % "1.8",
      "io.argonaut"                   %% "argonaut"                          % "6.2.1",
      "de.heikoseeberger"             %% "akka-http-argonaut"                % "1.20.0",
      "io.kamon"                      %% "kamon-core"                        % "1.1.0"
    ),
    javaAgents += "org.aspectj" % "aspectjweaver" % "1.8.13", // (2)
    javaOptions in Universal += "-Dorg.aspectj.tracing.factory=default" // (3)
  )
  .dependsOn(`kafka-lib`, commons)
  .enablePlugins(JavaAppPackaging, JavaAgent)
  .enablePlugins(DockerPlugin)
  .enablePlugins(GitVersioning)

lazy val `kafka-lib` = project
  .settings(
    scalafmtOnCompile in ThisBuild := true,
    scalafmtTestOnCompile in ThisBuild := true,
    scalacOptions ++= compilerFlags,
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-stream-typed" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream-kafka" % "0.19"
    )
  )
  .dependsOn(commons)

lazy val commons = project
  .settings(
    scalafmtOnCompile in ThisBuild := true,
    scalafmtTestOnCompile in ThisBuild := true,
    scalacOptions ++= compilerFlags,
    libraryDependencies ++= Seq(
      "com.typesafe.scala-logging" %% "scala-logging"           % "3.8.0",
      "ch.qos.logback"             % "logback-classic"          % "1.2.3",
      "net.logstash.logback"       % "logstash-logback-encoder" % "5.0"
    )
  )

package com.aerohitsaxena.logging

import com.typesafe.scalalogging.LazyLogging

trait LoggingBehaviour extends LazyLogging

package com.aerohitsaxena.kafka

import akka.NotUsed
import akka.actor.ActorSystem
import akka.actor.typed.ActorRef
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.ActorMaterializer
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.Flow
import akka.stream.typed.scaladsl.ActorSource
import com.aerohitsaxena.logging.LoggingBehaviour
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer

object KafkaProducer extends LoggingBehaviour {
  def create[E](
    topic: String,
    completionMatcher: PartialFunction[E, Unit],
    failureMatcher: PartialFunction[E, Throwable]
  )(implicit marshaller: KafkaMarshaller[E], system: ActorSystem, mat: ActorMaterializer): ActorRef[E] = {
    val producerSettings =
      ProducerSettings(system, new StringSerializer, new StringSerializer)
        .withBootstrapServers("kafka-service:9092") // TODO: this should be configurable
    ActorSource
      .actorRef[E](
        completionMatcher = completionMatcher,
        failureMatcher = failureMatcher,
        100, // TODO: This should be configurable
        overflowStrategy = OverflowStrategy.fail
      )
      .via(log)
      .via(toProducerRecord(topic))
      .to(Producer.plainSink(producerSettings))
      .run()
  }

  def create[E](
    topic: String
  )(implicit marshaller: KafkaMarshaller[E], system: ActorSystem, mat: ActorMaterializer): ActorRef[E] = {
    create(topic, doNothingCompletionMatcher(), doNothingFailureMatcher())
  }

  private def log[E]: Flow[E, E, NotUsed] = Flow[E].map { e =>
    // TODO: use actual logger
    logger.info(s"**** Will publish this to kafka: $e")
    e
  }

  private def toProducerRecord[E](topic: String)(
    implicit marshaller: KafkaMarshaller[E]): Flow[E, ProducerRecord[String, String], NotUsed] =
    Flow[E].map(e => new ProducerRecord(topic, marshaller.toProducerString(e)))

//  private def toUnit[E]: PartialFunction[E, Unit] = {
  private def doNothingCompletionMatcher[E]() = new PartialFunction[E, Unit] {
    override def isDefinedAt(x: E): Boolean = false

    override def apply(v1: E): Unit = {}
  }

  private def doNothingFailureMatcher[E]() = new PartialFunction[E, Throwable] {
    override def isDefinedAt(x: E): Boolean = false

    override def apply(v1: E): Throwable = new RuntimeException("THIS SHOULD NEVER HAVE BEEN CALLED")
  }
}

trait KafkaMarshaller[T] {
  def toProducerString(msg: T): String
}
